from typing import Tuple

V3 = Tuple[float, float, float]
Color = Tuple[int, int, int] | Tuple[int, int, int, int]
