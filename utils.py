from math import cos, sin
from typing import Tuple
from typing_utils import V3


def rotation_matrix(alpha: float,
                    beta: float,
                    gamma: float) -> Tuple[V3, V3, V3]:
    """
    Rotation matrix of α, β, γ radians around x, y, z axes (respectively)
    """
    alpha_sin, alpha_cos = sin(alpha), cos(alpha)
    beta_sin, beta_cos = sin(beta), cos(beta)
    gamma_sin, gamma_cos = sin(gamma), cos(gamma)
    return (
        (beta_cos*gamma_cos,
         -beta_cos*gamma_sin,
         beta_sin),
        (alpha_cos*gamma_sin + alpha_sin*beta_sin*gamma_cos,
         alpha_cos*gamma_cos - gamma_sin*alpha_sin*beta_sin,
         -beta_cos*alpha_sin),
        (gamma_sin*alpha_sin - alpha_cos*beta_sin*gamma_cos,
         alpha_cos*gamma_sin*beta_sin + alpha_sin*gamma_cos,
         alpha_cos*beta_cos)
    )
