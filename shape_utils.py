from math import sqrt, cos, sin, pi
from typing import Collection, Set

from pygame import Surface
from numpy import linspace
from typing_utils import Color, V3

from shape import Shape


def make_edges(base_vertices: Collection[V3]) -> Collection[Set[int]]:
    return tuple(
        {i + 1, 0} for i in range(len(base_vertices))
    ) + tuple(
        {i + 1, ((i + 1) % len(base_vertices)) + 1}
        for i in range(len(base_vertices))
    )


def make_faces(base_vertices: Collection[V3]) -> Collection[Set[int]]:
    return tuple(
        {0, i + 1, ((i + 1) % len(base_vertices)) + 1}
        for i in range(len(base_vertices))
    ) + (tuple(range(1, len(base_vertices) + 1)),)


def make_pyramid(base_inner_radius: float,
                 height: float,
                 color: Color,
                 position,
                 surface: Surface,
                 scale_factor: float = 1) -> Shape:
    # outer radius is a hypotenuse, inner radius is a cathetus facing 30* angle
    base_outer_radius = base_inner_radius * 2
    base_side_length = base_outer_radius * sqrt(3)
    half = base_side_length * 0.5
    base_vertices = (
        (0.0, height * 0.5, base_outer_radius),
        (half, height * 0.5, -base_inner_radius),
        (-half, height * 0.5, -base_inner_radius)
    )
    apex = (0, -height * 0.5, 0)
    return Shape(
        vertices=(apex,) + base_vertices,
        edges=make_edges(base_vertices),
        faces=make_faces(base_vertices),
        position=position,
        color=color,
        surface=surface,
        scale_factor=scale_factor
    )


def make_cone(base_radius: float,
              height: float,
              color: Color,
              position,
              surface: Surface,
              scale_factor: float = 1,
              n_base_points: int = 100) -> Shape:
    base_vertices = tuple(
        (base_radius * cos(theta), height * 0.5, base_radius * sin(theta))
        for theta in linspace(0, 2 * pi, n_base_points)
    )
    apex = (0.0, -height * 0.5, 0.0)
    return Shape(
        vertices=(apex,) + base_vertices,
        edges=make_edges(base_vertices)[len(base_vertices):],
        faces=make_faces(base_vertices),
        position=position,
        color=color,
        surface=surface,
        scale_factor=scale_factor
    )