from typing import Tuple, Iterable, Set, Collection, List

from pygame import Surface
from pygame.draw import polygon as draw_polygon, line as draw_line
from numpy import array
from utils import rotation_matrix
from typing_utils import V3, Color


class Shape:
    def __init__(self,
                 vertices: Collection[V3],
                 edges: Iterable[Set[int]],
                 faces: Iterable[Set[int]],
                 color: Tuple[int, int, int, int],
                 surface: Surface,
                 position: List[float],
                 scale_factor: float = 1):
        """
        A 3D object that can rotate around the three axes

        :param vertices: a tuple of points (each has 3 coordinates)
        :param edges: a tuple of pairs (each pair is a set containing 2 vertices' indexes)
        """
        self.__vertices = array(vertices)
        self.__edges = tuple(edges)
        self.__faces = tuple(faces)
        self.__surface = surface
        self.__position = position
        self.__rotation = [0, 0, 0]  # radians around each axis
        self.scale_factor = scale_factor
        self.color = color

    def rotate(self, axis: int, theta: float) -> None:
        self.__rotation[axis] += theta

    def translate(self, axis: int, delta: float) -> None:
        self.__position[axis] += delta

    def reset_rotation(self) -> None:
        self.__rotation = [0, 0, 0]

    def fit(self, vector: V3) -> Tuple[float, float]:
        """
        ignore the z-element (creating a very cheap projection), and scale x, y to the coordinates of the screen
        """
        return tuple(round(self.scale_factor * coordinate + origin)
                     for coordinate, origin in zip(vector, self.__position))

    @property
    def lines(self) -> Iterable[Tuple[V3, V3, Color]]:
        location = self.__vertices.dot(rotation_matrix(*self.__rotation))
        return ((location[v1], location[v2], self.color) for v1, v2 in self.__edges)

    @property
    def faces(self) -> Iterable[Tuple[Iterable[V3], Color]]:
        location = self.__vertices.dot(rotation_matrix(*self.__rotation))
        return ((tuple(location[i] for i in face), self.color) for face in self.__faces)

    def draw(self, surface: Surface,
             scale_factor: float = 1.0,
             edge_thickness: int = 4) -> None:
        self.__surface.fill((0, 0, 0, 0))
        for points, color in self.faces:
            fit_points = tuple(map(self.fit, points))
            fill_color = tuple(round(c * 0.8) for c in color[:3]) + (color[3],)
            draw_polygon(self.__surface, fill_color, fit_points)
        for start, end, color in self.lines:
            draw_line(self.__surface, color, self.fit(start),
                      self.fit(end), edge_thickness)
        surface.blit(self.__surface, (0, 0))
