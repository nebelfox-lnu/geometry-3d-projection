import pygame
from pygame import K_r, K_ESCAPE, K_q, K_w, K_a, K_s, K_d, K_e, K_z, K_x
from pygame import K_UP, K_DOWN, K_LEFT, K_RIGHT

from shape_utils import make_cone, make_pyramid

# Axes
X, Y, Z = 0, 1, 2
# Viewport
VIEWPORT_SIZE = (800, 600)
FPS = 30
# Colors
BLACK = (0, 0, 0)
TRANSPARENT = (0, 0, 0, 0)
CONE_COLOR = (0, 255, 0, 160)
PYRAMID_COLOR = (255, 0, 0, 128)
# Shape parameters
PYRAMID_HEIGHT = 4
PYRAMID_INNER_RADIUS = 2
CONE_HEIGHT = PYRAMID_HEIGHT
CONE_RADIUS = PYRAMID_INNER_RADIUS
# Transform
CLOCKWISE_ROTATION_SPEED = -0.025
COUNTER_CLOCKWISE_ROTATION_SPEED = -CLOCKWISE_ROTATION_SPEED
ROTATION_KEY_BINDINGS = {
    K_s: (X, CLOCKWISE_ROTATION_SPEED),
    K_w: (X, COUNTER_CLOCKWISE_ROTATION_SPEED),
    K_d: (Y, CLOCKWISE_ROTATION_SPEED),
    K_a: (Y, COUNTER_CLOCKWISE_ROTATION_SPEED),
    K_e: (Z, CLOCKWISE_ROTATION_SPEED),
    K_q: (Z, COUNTER_CLOCKWISE_ROTATION_SPEED)
}
INITIAL_SCALE_FACTOR = 70
SCALE_INCREASE_SPEED = 1.0
SCALE_DECREASE_SPEED = -SCALE_INCREASE_SPEED
MOVE_SPEED = 5
MOVE_KEY_BINDINGS = {
    K_UP: (Y, -MOVE_SPEED),
    K_DOWN: (Y, MOVE_SPEED),
    K_LEFT: (X, -MOVE_SPEED),
    K_RIGHT: (X, MOVE_SPEED)
}


def main():
    pygame.init()
    pygame.display.set_caption('X: w|s; Y: a|d; Z: q|e; Scale: z|x; Move: arrows')
    clock = pygame.time.Clock()
    screen = pygame.display.set_mode(VIEWPORT_SIZE)
    surface = pygame.Surface(VIEWPORT_SIZE, pygame.SRCALPHA)

    center = [VIEWPORT_SIZE[0] * 0.5, VIEWPORT_SIZE[1] * 0.5]
    cone = make_cone(CONE_RADIUS,
                     CONE_HEIGHT,
                     CONE_COLOR,
                     center,
                     pygame.Surface(VIEWPORT_SIZE, pygame.SRCALPHA),
                     INITIAL_SCALE_FACTOR)
    pyramid = make_pyramid(PYRAMID_INNER_RADIUS,
                           PYRAMID_HEIGHT,
                           PYRAMID_COLOR,
                           center,
                           pygame.Surface(VIEWPORT_SIZE, pygame.SRCALPHA),
                           INITIAL_SCALE_FACTOR)
    shapes = (cone, pyramid)

    def handle_events():
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                exit()
        keys = pygame.key.get_pressed()
        if keys[K_ESCAPE]:
            exit()
        if keys[K_r]:
            for shape in shapes:
                shape.reset_rotation()
        if keys[K_z]:
            for shape in shapes:
                shape.scale_factor += SCALE_DECREASE_SPEED
        if keys[K_x]:
            for shape in shapes:
                shape.scale_factor += SCALE_INCREASE_SPEED
        for key in ROTATION_KEY_BINDINGS:
            if keys[key]:
                params = ROTATION_KEY_BINDINGS[key]
                for shape in shapes:
                    shape.rotate(*params)
        for key in MOVE_KEY_BINDINGS:
            if keys[key]:
                params = MOVE_KEY_BINDINGS[key]
                for shape in shapes:
                    shape.translate(*params)

    while True:
        handle_events()
        screen.fill(BLACK)
        surface.fill(TRANSPARENT)
        for shape in shapes:
            shape.draw(surface)
        screen.blit(surface, (0, 0))
        pygame.display.flip()
        clock.tick(FPS)


if __name__ == '__main__':
    main()
