# 3D Projection

The project implements basic projection of a 3D object to 2D surface,
addressing the respective challenge encountered during 6th semester
of Applied Mathematics Bachelor Degree.

The project uses [pygame](https://www.pygame.org/news) for graphics.
The capabilities are to move, zoom and rotate the rendered object in real-time.
